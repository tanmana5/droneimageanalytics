"""This code is a first experiment to find tree regions in a drone image.

The code is tested using OpenCV 2.0/3.0 and Python version 2.7.
The basic setup of the code is as follows:
- Find the range of hue values of the shade of green observed in majority of
the drone images
- Apply a mask/filter to perform a segmentation based on the above values to
detect green forest regions as opposed to burn/ ground regions.
"""
import os
import numpy as np
import argparse
import cv2


def preprocess(image):
    image_blur = cv2.GaussianBlur(image, (5, 5), 0)
    # unlike RGB, HSV separates luma, or the image intensity, from
    # chroma or the color information.
    image_blur_hsv = cv2.cvtColor(image_blur, cv2.COLOR_RGB2HSV)
    return image_blur_hsv


def morphological(mask):

    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    mask_closed = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
    # erosion followed by dilation. 
    mask_final = cv2.morphologyEx(mask_closed, cv2.MORPH_OPEN, kernel)
    return mask_final


def filter_by_colour(image):
    min_green = np.array([70, 70, 60])
    max_green = np.array([128, 255, 85])
    mask1 = cv2.inRange(image, min_green, max_green)
    # extracting hue, or brightness of a color 
    min_green2 = np.array([0, 70, 0])
    max_green2 = np.array([90, 256, 85])
    mask2 = cv2.inRange(image, min_green2, max_green2)
    # Combine masks
    mask_final = mask1 + mask2
    # mask_final = morphological(mask_final)
    return mask_final


# this function finds green tree areas using a color thresholding.
def find_tree_regions(path, listing):
    for infile in listing:
        print("current file being processed: " + infile)
        # load the image
        image = cv2.imread(path+infile)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        scale = 0.2
        image = cv2.resize(image, (0, 0), fx=scale, fy=scale)
        # image = preprocess(image)
        mask = filter_by_colour(image)
        # find the colors within the specified boundaries and apply
        # the mask
        output = cv2.bitwise_and(image, image, mask=mask)
        # show the images
        output = cv2.resize(image, (0, 0), fx=0.5, fy=0.5)
        output = cv2.cvtColor(output, cv2.COLOR_RGB2BGR)
        cv2.imshow("outputs", np.hstack([output, image]))
        cv2.imwrite(path+infile+"_out.jpg", output)
        cv2.waitKey(0)


# to fill in count approximation function
def calc_approx_no_of_trees(image):
    # estimate = tot_area_of_green_pixels/avg_area_of_tree
    estimate = 0
    return estimate


def main():
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-p", "--path", help="path to the folder containing images")
    args = vars(ap.parse_args())
    path = args["path"]
    listing = os.listdir(path)
    find_tree_regions(path, listing)


if __name__ == main():
    main()