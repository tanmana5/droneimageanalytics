# DroneImageAnalytics

This code repo is for drone image analysis.
The datasets consist of real-world aerial footage. Datasets are not available publicly.



Sample original image below
![Altext](images/DJI_0672.JPG)



Sample processed image below
![Alttext](images/DJI_0672.JPG_out.jpg)


Repo moved to Gitlab.
